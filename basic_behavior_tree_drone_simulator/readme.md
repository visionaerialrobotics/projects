##  Behavior tree mission with drone simulator

In order to execute the mission defined in configs/drone111/behavior_tree_mission_file.yaml, perform the following steps:

- Execute the roscore:

        $ roscore

- Execute the script that launches the Aerostack components for this project:

        $ ./main_launcher.sh

![behavior_tree_mission.png](https://bitbucket.org/repo/rokr9B/images/2647577933-behavior_tree_mission.png)


- Press the 'Start mission' button to start executing the mission.


Here there is a video that shows the correct execution of the mission:

[ ![Behavior Tree Mission](https://img.youtube.com/vi/iY9D1sBC4u4/0.jpg)](https://www.youtube.com/watch?v=iY9D1sBC4u4)
