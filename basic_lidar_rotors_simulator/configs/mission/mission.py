#!/usr/bin/env python2

import executive_engine_api as api
import rospy
import numpy as np
from aerostack_msgs.msg import ListOfBeliefs
import time
import math

p1_points = [
  [2.2, 2.2, 1]
]

p2_points = [
  [-1.5, -1.5, 1]
]

p3_points = [
  [2.2, -3.2, 1]
]

p4_points = [
  [0, 0, 1]
]

points = [
  p1_points,
  p2_points,
  p3_points,
  p4_points
]


def runMission():
  print("Starting mission...")
  print("Taking off...") 
  j=1
  api.executeBehavior('TAKE_OFF')
  print("Localizating off...")
  api.activateBehavior('SELF_LOCALIZE_AND_MAP_WITH_LIDAR')
  print("Moving...")
  for destination in points:
    for point in destination:
      result = api.executeBehavior('GENERATE_PATH_WITH_OCCUPANCY_GRID', coordinates=point)
      print("............")
      query = 'path ('+str(j)+',?y)'.format()
      success , unification = api.consultBelief(query)
      print query
      print success
      if success:
         traject = unification['y']
         traject = eval(traject)
         traject = list(traject)
         result = [] 
         i=0
         pointTraj=traject[0]
         dist=0.75
         while i < len(traject)-1: 
                 pointTraj= traject[i]
                 result.append(list(traject[i])) 
                 i+=1
         result.append(list(traject[len(traject)-1])) 
         print len(result)
         print result
         print "Following path"
         print "---------------------------------"
         time.sleep(3)
      j+=1
      result = api.executeBehavior('FOLLOW_PATH_WITH_PID_CONTROL', path=result)
  result = api.executeBehavior('LAND')
  print('Finish mission...')