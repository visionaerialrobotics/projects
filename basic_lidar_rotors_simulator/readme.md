##  basic_lidar_rotors_simulator

This test verifies the correct execution of a mission (written in Python) with Rotors simulator while using Lidar, and it can only be run in ubuntu 16 with ros kinetic.

In order to execute this test, perform the following steps:

- Install the Aerostack project called "basic_lidar_navigation_with_rotors_simulation".

- Change directory to this project:

        $ cd basic_lidar_navigation_with_rotors_simulation

- This project uses the planner "move_base" and "hector_slam", provided as a general ROS package. To install "move_base" and "hector_slam" from ROS write the following command (in this example we are using the ROS distribution ros-melodic, but you can use another distribution instead):

        $ sudo apt-get install ros-melodic-move-base*

        $ sudo apt-get install ros-melodic-hector*
     
        $ sudo apt-get install ros-melodic-amcl*

- Install the components:

        $ ./lidar_instalation.sh

- Execute the script that launches Gazebo for this project:

        $ ./launcher_gazebo.sh

- Execute the script that launches the Aerostack components for this project:

        $ ./main_launcher.sh

- In order to run the mission you have to execute the following command:

        $ rosservice call /drone111/python_based_mission_interpreter_process/start

Here there is a video that shows the correct execution of the test:

[ ![Python Mission](https://img.youtube.com/vi/OIGQ_cGDhEA/0.jpg)](https://youtu.be/OIGQ_cGDhEA)
