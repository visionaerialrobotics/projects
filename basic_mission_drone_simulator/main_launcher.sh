#!/bin/bash

NUMID_DRONE=111
DRONE_SWARM_ID=1
DRONE_IP=192.168.1.1
export AEROSTACK_PROJECT=${AEROSTACK_STACK}/projects/basic_mission_drone_simulator

. ${AEROSTACK_STACK}/setup.sh
OPEN_ROSCORE=1

#---------------------------------------------------------------------------------------------
# INTERNAL PROCESSES
#---------------------------------------------------------------------------------------------
gnome-terminal  \
`#---------------------------------------------------------------------------------------------` \
`# Quadrotor simulator                                                                         ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Simulator" --command "bash -c \"
roslaunch droneSimulatorROSModule droneSimulatorROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# EKF localization                                                                            ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Robot localization" --command "bash -c \"
roslaunch droneEKFStateEstimatorROSModule droneEKFStateEstimatorROSModule.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Drone Trajectory Controller                                                                 ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Drone Trajectory Controller" --command "bash -c \"
roslaunch droneTrajectoryControllerROSModule droneTrajectoryControllerROSModule.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE  \
  drone_estimated_pose_topic_name:=EstimatedPose_droneGMR_wrt_GFF \
  drone_estimated_speeds_topic_name:=EstimatedSpeed_droneGMR_wrt_GFF \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Belief Manager                                                                              ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Belief Manager" --command "bash -c \"
roslaunch belief_manager_process belief_manager_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT} \
  config_path:=${AEROSTACK_PROJECT}/configs/drone$NUMID_DRONE;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Belief Updater                                                                              ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Belief Updater" --command "bash -c \"
roslaunch belief_updater_process belief_updater_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Process manager                                                                             ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Process manager" --command "bash -c \"
roslaunch process_manager_process process_manager_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Python Interpreter                                                                          ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Python Interpreter" --command "bash -c \"
roslaunch python_based_mission_interpreter_process python_based_mission_interpreter_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT} \
  mission_configuration_folder:=${AEROSTACK_PROJECT}/configs/mission ;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Pose Adapter                                                                                ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Pose Adapter" --command "bash -c \"
roslaunch pose_adapter_process pose_adapter_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Execution Viewer                                                                                ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Execution Viewer" --command "bash -c \"
roslaunch execution_viewer execution_viewer.launch --wait \
  robot_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Self Localization Selector Process                                                          ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Self Localization Selector" --command "bash -c \"
roslaunch self_localization_selector_process self_localization_selector_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT} \
    aruco_slam_estimated_speed_topic:=EstimatedSpeed_droneGMR_wrt_GFF;
exec bash\""  &

gnome-terminal \
`#---------------------------------------------------------------------------------------------` \
`# Basic Quadrotor Behaviors                                                                   ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Basic Quadrotor Behaviors" --command "bash -c \"
roslaunch basic_quadrotor_behaviors basic_quadrotor_behaviors.launch --wait \
  namespace:=drone$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""\
`#---------------------------------------------------------------------------------------------` \
`# Quadrotor Motion With PID Control Behaviors                                                 ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title " Quadrotor Motion With PID Control Behaviors" --command "bash -c \"
roslaunch quadrotor_motion_with_trajectory_controller quadrotor_motion_with_trajectory_controller.launch --wait \
  namespace:=drone$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\"" &

echo "- Waiting for all process to be started..."
# wait for the modules to be running, the trick here is that rostopics blocks the execution
# until the message topic is up and running and delivers messages
rostopic echo -n 1 /drone$NUMID_DRONE/droneTrajectoryController/controlMode &> /dev/null
rosservice call /drone$NUMID_DRONE/droneRobotLocalizationROSModuleNode/start

gnome-terminal \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Coordinator                                                                        ` \
`#---------------------------------------------------------------------------------------------` \
"Behavior Coordinator" --command "bash -c \"
roslaunch behavior_coordinator_process behavior_coordinator_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  behavior_catalog_path:=${AEROSTACK_PROJECT}/configs/mission/behavior_catalog.yaml \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  &


#---------------------------------------------------------------------------------------------
# USER INTERFACE PROCESSES
#---------------------------------------------------------------------------------------------
xfce4-terminal \
`#---------------------------------------------------------------------------------------------` \
`# RViz Interface                                                                              ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "RViz Interface"  --command "bash -c \"
roslaunch droneArchitectureRvizROSModule droneArchitectureRvizInterfaceROSModule.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    config_file:=${AEROSTACK_PROJECT}/configs/rviz_config.rviz \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  &
