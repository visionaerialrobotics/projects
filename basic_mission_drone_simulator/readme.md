##  Basic mission with drone simulator

In order to execute the mission defined in configs/drone111/mission.py, perform the following steps:

- Execute the roscore:

        $ roscore

- Execute the script that launches the Aerostack components for this project:

        $ ./main_launcher.sh

![rviz.png](https://bitbucket.org/repo/rokr9B/images/4149553524-rviz.png)


- Execute the following command to run the mission:

        $ rosservice call /drone111/python_based_mission_interpreter_process/start

Here there is a video that shows the correct execution of the mission:

[ ![Drone Simulator Mission](https://img.youtube.com/vi/sOgy5IW3aEc/0.jpg)](https://www.youtube.com/watch?v=sOgy5IW3aEc)
