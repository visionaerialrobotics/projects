[submodule "stack/libraries/lib_cvgekf"]
	path = stack/libraries/lib_cvgekf
	url = https://bitbucket.org/joselusl/lib_cvgekf.git
[submodule "stack/libraries/lib_cvgutils"]
	path = stack/libraries/lib_cvgutils
	url = https://bitbucket.org/jespestana/lib_cvgutils.git
[submodule "stack/libraries/lib_newmat11"]
	path = stack/libraries/lib_newmat11
	url = https://bitbucket.org/joselusl/lib_newmat11.git
[submodule "stack/libraries/lib_pugixml"]
	path = stack_deprecated/libraries/lib_pugixml
	url = https://bitbucket.org/joselusl/lib_pugixml.git
[submodule "stack/droneDrivers/driversPlatforms/driverBebopDrone/bebop_autonomy"]
	path = stack/hardware_interface/drivers_platforms/driver_bebop/bebop_autonomy
	url = https://github.com/AutonomyLab/bebop_autonomy.git
[submodule "stack/libraries/pugixml"]
	path = stack/libraries/pugixml
	url = https://github.com/joselusl/pugixml
[submodule "stack/hardware_interface/drivers_platforms/driver_pixhawk/mavros"]
	path = stack/hardware_interface/drivers_platforms/driver_pixhawk/mavros
	url = https://github.com/mavlink/mavros.git
[submodule "stack/hardware_interface/drivers_sensors/driver_px4flow/driver_px4flow_process"]
	path = stack/hardware_interface/drivers_sensors/driver_px4flow/driver_px4flow_process
	url = https://bitbucket.org/jespestana/driver_px4flow_interface_rosmodule.git
[submodule "stack/hardware_interface/drivers_sensors/driver_px4flow/px-ros-pkg"]
	path = stack/hardware_interface/drivers_sensors/driver_px4flow/px-ros-pkg
	url = https://github.com/cvg/px-ros-pkg.git
[submodule "stack/ground_control_system/cli/cli_process"]
	path = stack/ground_control_system/cli/cli_process
	url = https://bitbucket.org/joselusl/droneinterfacerosmodule.git
[submodule "stack_obsolete/libraries/lib_pose"]
	path = stack_deprecated/libraries/lib_pose
	url = https://bitbucket.org/joselusl/lib_pose.git
[submodule "stack_obsolete/libraries/referenceFrames"]
	path = stack_deprecated/libraries/referenceFrames
	url = https://bitbucket.org/joselusl/referenceframes.git
[submodule "stack_obsolete/libraries/lib_cvgthread"]
	path = stack_deprecated/libraries/lib_cvgthread
	url = https://bitbucket.org/jespestana/lib_cvgthread.git
[submodule "stack/supervision_system/process_monitor/process_monitor_process"]
	path = stack/process_management_system/process_monitor/process_monitor_process
	url = https://bitbucket.org/visionaerialrobotics/process_monitor_process.git
[submodule "stack/common/drone_process"]
	path = stack_deprecated/common/drone_process
	url = https://bitbucket.org/visionaerialrobotics/drone_process.git
[submodule "stack/common/drone_module"]
	path = stack_deprecated/common/drone_module
	url = https://bitbucket.org/joselusl/dronemoduleros.git
[submodule "stack_deprecated/common/drone_module_interface"]
	path = stack_deprecated/common/drone_module_interface
	url = https://bitbucket.org/jespestana/dronemoduleinterfaceros.git
[submodule "stack/feature_extraction_system/opentld/ros_opentld"]
	path = stack/feature_extraction_system/opentld/ros_opentld
	url = https://github.com/hridaybavle/ros_opentld
[submodule "stack/libraries/eigen_catkin"]
	path = stack/libraries/eigen_catkin
	url = https://github.com/suarez-ramon/eigen_catkin
[submodule "stack/libraries/catkin_simple"]
	path = stack/libraries/catkin_simple
	url = https://github.com/suarez-ramon/catkin_simple
[submodule "stack/common/behavior_process"]
	path = stack/common/behavior_process
	url = https://bitbucket.org/visionaerialrobotics/behavior_process
[submodule "stack/common/robot_process"]
	path = stack/common/robot_process
	url = https://bitbucket.org/visionaerialrobotics/robot_process
[submodule "stack/process_management_system/process_manager/process_manager_process"]
	path = stack/process_management_system/process_manager/process_manager_process
	url = https://bitbucket.org/visionaerialrobotics/process_manager_process.git
[submodule "stack_devel/common/aerostack_msgs"]
	path = stack/common/aerostack_msgs
	url = https://bitbucket.org/visionaerialrobotics/aerostack_msgs.git
[submodule "stack/common/msgs_adapters"]
	path = stack/common/msgs_adapters
	url = https://bitbucket.org/visionaerialrobotics/msgs_adapters.git
[submodule "stack/common/behavior_execution_controller"]
	path = stack/common/behavior_execution_controller
	url = https://bitbucket.org/visionaerialrobotics/behavior_execution_controller.git
[submodule "stack/hardware_interface/drivers_platforms/driver_rotors_simulator/driver_rotors_simulator_process"]
	path = stack/hardware_interface/drivers_platforms/driver_rotors_simulator/driver_rotors_simulator_process
	url = https://bitbucket.org/visionaerialrobotics/driver_rotors_simulator_process.git
[submodule "stack_deprecated/libraries/mav_comm_rotors"]
	path = stack_deprecated/libraries/mav_comm_rotors
	url = https://github.com/acgroba/mav_comm_rotors.git
[submodule "stack/mission_control_system/python_based_mission_interpreter/python_based_mission_interpreter_process"]
	path = stack/mission_control_system/python_based_mission_interpreter/python_based_mission_interpreter_process
	url = https://bitbucket.org/visionaerialrobotics/python_based_mission_interpreter_process.git
[submodule "stack/belief_management_system/belief_manager/belief_manager"]
	path = stack/belief_management_system/belief_manager/belief_manager
	url = https://bitbucket.org/visionaerialrobotics/belief_manager.git
[submodule "stack/belief_management_system/belief_manager/belief_manager_process"]
	path = stack/belief_management_system/belief_manager/belief_manager_process
	url = https://bitbucket.org/visionaerialrobotics/belief_manager_process.git
[submodule "stack/belief_management_system/belief_updater/belief_updater_process"]
	path = stack/belief_management_system/belief_updater/belief_updater_process
	url = https://bitbucket.org/visionaerialrobotics/belief_updater_process.git
[submodule "stack_deprecated/common/droneMsgsROS"]
	path = stack_deprecated/common/droneMsgsROS
	url = https://bitbucket.org/joselusl/dronemsgsros.git
[submodule "stack_deprecated/logging/droneLoggerROSModule"]
	path = stack_deprecated/logging/droneLoggerROSModule
	url = https://bitbucket.org/jespestana/droneloggerrosmodule.git
[submodule "stack_deprecated/logging/lib_cvglogger"]
	path = stack_deprecated/logging/lib_cvglogger
	url = https://bitbucket.org/jespestana/lib_cvglogger.git
[submodule "stack_deprecated/logging/lib_cvgloggerROS"]
	path = stack_deprecated/logging/lib_cvgloggerROS
	url = https://bitbucket.org/jespestana/lib_cvgloggerros.git
[submodule "stack/localization_and_mapping_system/odometry_pose_estimator/odometry_pose_estimator"]
	path = stack/localization_and_mapping_system/odometry_pose_estimator/odometry_pose_estimator
	url = https://bitbucket.org/jespestana/droneekfstateestimator.git
[submodule "stack/localization_and_mapping_system/odometry_pose_estimator/odometry_pose_estimator_process"]
	path = stack/localization_and_mapping_system/odometry_pose_estimator/odometry_pose_estimator_process
	url = https://bitbucket.org/jespestana/droneekfstateestimatorrosmodule.git
[submodule "stack/localization_and_mapping_system/robot_localization/droneRobotLocalizationROSModule"]
	path = stack/localization_and_mapping_system/robot_localization/droneRobotLocalizationROSModule
	url = https://bitbucket.org/hridaybavle/dronerobotlocalizationrosmodule.git
[submodule "stack/localization_and_mapping_system/robot_localization/robot_localization"]
	path = stack/localization_and_mapping_system/robot_localization/robot_localization
	url = https://github.com/cra-ros-pkg/robot_localization.git
[submodule "stack/localization_and_mapping_system/self_localization_selector/self_localization_selector_process"]
	path = stack/localization_and_mapping_system/self_localization_selector/self_localization_selector_process
	url = https://bitbucket.org/visionaerialrobotics/self_localization_selector_process.git
[submodule "launchers"]
	path = launchers
	url = https://bitbucket.org/visionaerialrobotics/launchers.git
[submodule "stack/motion_control_system/basic_actions_controller/droneMidLevelAutopilot"]
	path = stack/motion_control_system/basic_actions_controller/droneMidLevelAutopilot
	url = https://bitbucket.org/jespestana/dronemidlevelautopilot.git
[submodule "stack/motion_control_system/basic_actions_controller/droneMidLevelAutopilotROSModule"]
	path = stack/motion_control_system/basic_actions_controller/droneMidLevelAutopilotROSModule
	url = https://bitbucket.org/jespestana/dronemidlevelautopilotrosmodule.git
[submodule "stack/motion_control_system/trajectory_controller/dronePBVSPositionMidLevelController"]
	path = stack/motion_control_system/trajectory_controller/dronePBVSPositionMidLevelController
	url = https://bitbucket.org/jespestana/dronepbvspositionmidlevelcontroller.git
[submodule "stack/motion_control_system/trajectory_controller/droneSpeedPositionMidLevelController"]
	path = stack/motion_control_system/trajectory_controller/droneSpeedPositionMidLevelController
	url = https://bitbucket.org/jespestana/dronespeedpositionmidlevelcontroller.git
[submodule "stack/motion_control_system/trajectory_controller/droneTrajectoryController"]
	path = stack/motion_control_system/trajectory_controller/droneTrajectoryController
	url = https://bitbucket.org/jespestana/dronetrajectorycontroller.git
[submodule "stack/motion_control_system/trajectory_controller/droneTrajectoryControllerROSModule"]
	path = stack/motion_control_system/trajectory_controller/droneTrajectoryControllerROSModule
	url = https://bitbucket.org/jespestana/dronetrajectorycontrollerrosmodule.git
[submodule "stack/localization_and_mapping_system/visual_markers_localizer/reference_frames"]
	path = stack/localization_and_mapping_system/visual_markers_localizer/reference_frames
	url = https://bitbucket.org/visionaerialrobotics/reference_frames.git
[submodule "stack/localization_and_mapping_system/visual_markers_localizer/visual_markers_localizer"]
	path = stack/localization_and_mapping_system/visual_markers_localizer/visual_markers_localizer
	url = https://bitbucket.org/visionaerialrobotics/visual_markers_localizer.git
[submodule "stack/localization_and_mapping_system/visual_markers_localizer/visual_markers_localizer_process"]
	path = stack/localization_and_mapping_system/visual_markers_localizer/visual_markers_localizer_process
	url = https://bitbucket.org/visionaerialrobotics/visual_markers_localizer_process.git
[submodule "stack/motion_control_system/dronetrajectorystatemachine"]
	path = stack/motion_control_system/dronetrajectorystatemachine
	url = https://bitbucket.org/visionaerialrobotics/dronetrajectorystatemachine.git
[submodule "stack/execution_control_system/behavior_coordinator/behavior_coordinator_process"]
	path = stack/execution_control_system/behavior_coordinator/behavior_coordinator_process
	url = https://bitbucket.org/visionaerialrobotics/behavior_coordinator_process.git
[submodule "stack/execution_control_system/behavior_systems/basic_quadrotor_behaviors"]
	path = stack/execution_control_system/behavior_systems/basic_quadrotor_behaviors
	url = https://bitbucket.org/visionaerialrobotics/basic_quadrotor_behaviors.git
[submodule "stack/execution_control_system/behavior_systems/quadrotor_motion_with_trajectory_controller"]
	path = stack/execution_control_system/behavior_systems/quadrotor_motion_with_trajectory_controller
	url = https://bitbucket.org/visionaerialrobotics/quadrotor_motion_with_trajectory_controller.git
[submodule "stack/simulation_system/rotors_simulator"]
	path = stack/simulation_system/rotors_simulator
	url = https://bitbucket.org/visionaerialrobotics/rotors_simulator.git
