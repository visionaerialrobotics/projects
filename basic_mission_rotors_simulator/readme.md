
##  Basic mission with Rotors simulator

In order to execute the mission defined in configs/drone111/mission.py, perform the following steps:

- Execute the script that launches Gazebo for this project:

        $ ./launcher_gazebo.sh

![gazebo_launched.png](https://bitbucket.org/repo/rokr9B/images/208602178-gazebo_launched.png)

- Execute the script that launches the Aerostack components for this project:

        $ ./main_launcher.sh
![main_launcher.png](https://bitbucket.org/repo/rokr9B/images/670050666-main_launcher.png)

- Execute the following command to run the mission:

        $ rosservice call /drone111/python_based_mission_interpreter_process/start

Here there is a video that shows the correct execution of the mission:

[ ![Rotors Simulator Mission](https://img.youtube.com/vi/wLo4T_6d_OI/0.jpg)](https://www.youtube.com/watch?v=wLo4T_6d_OI)

