#!/bin/bash

NUMID_DRONE=111
DRONE_SWARM_ID=1
DRONE_IP=192.168.1.1
export AEROSTACK_PROJECT=${AEROSTACK_STACK}/projects/bridge_inspection_rotors_simulator

. ${AEROSTACK_STACK}/setup.sh
OPEN_ROSCORE=1
#---------------------------------------------------------------------------------------------
# INTERNAL PROCESSES
#---------------------------------------------------------------------------------------------
gnome-terminal  \
`#---------------------------------------------------------------------------------------------` \
`# Driver Rotors                                                                               ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Driver Rotors" --command "bash -c \"
roslaunch driverRotorsSimulatorROSModule driverRotorsSimulatorROSModuleSim.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  drone_swarm_id:=$DRONE_SWARM_ID \
  mav_name:=hummingbird_adr \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Midlevel Controller                                                                         ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Midlevel Controller" --command "bash -c \"
roslaunch droneMidLevelAutopilotROSModule droneMidLevelAutopilotROSModule.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# RobotLocalizationROSModule                                                                  ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "RobotLocalizationROSModule" --command "bash -c \"
roslaunch droneRobotLocalizationROSModule droneRobotLocalizationROSModule.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# EKF localization                                                                            ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Robot localization" --command "bash -c \"
roslaunch ${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/launch_files/Ekf.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Drone Trajectory Controller                                                                 ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Drone Trajectory Controller" --command "bash -c \"
roslaunch droneTrajectoryControllerROSModule droneTrajectoryControllerROSModule.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE  \
  drone_estimated_pose_topic_name:=EstimatedPose_droneGMR_wrt_GFF \
  drone_estimated_speeds_topic_name:=EstimatedSpeed_droneGMR_wrt_GFF \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Belief Manager                                                                              ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Belief Manager" --command "bash -c \"
roslaunch belief_manager_process belief_manager_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  config_path:=${AEROSTACK_PROJECT}/configs/mission \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Belief Updater                                                                              ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Belief Updater" --command "bash -c \"
roslaunch belief_updater_process belief_updater_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Process manager                                                                             ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Process manager" --command "bash -c \"
roslaunch process_manager_process process_manager_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Pose Adapter                                                                                ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Pose Adapter" --command "bash -c \"
roslaunch pose_adapter_process pose_adapter_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Execution Viewer                                                                                ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Execution Viewer" --command "bash -c \"
roslaunch execution_viewer execution_viewer.launch --wait \
  robot_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Self Localization Selector Process                                                          ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Self Localization Selector" --command "bash -c \"
roslaunch self_localization_selector_process self_localization_selector_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT} \
    aruco_slam_estimated_speed_topic:=EstimatedSpeed_droneGMR_wrt_GFF;
exec bash\""  &

gnome-terminal \
`#---------------------------------------------------------------------------------------------` \
`# Basic Quadrotor Behaviors                                                                   ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Basic Quadrotor Behaviors" --command "bash -c \"
roslaunch basic_quadrotor_behaviors basic_quadrotor_behaviors.launch --wait \
  namespace:=drone$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""\
`#---------------------------------------------------------------------------------------------` \
`# Quadrotor Motion With PID Control Behaviors                                                 ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title " Quadrotor Motion With PID Control Behaviors" --command "bash -c \"
roslaunch quadrotor_motion_with_trajectory_controller quadrotor_motion_with_trajectory_controller.launch --wait \
  namespace:=drone$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\"" &

echo "- Waiting for all process to be started..."
# wait for the modules to be running, the trick here is that rostopics blocks the execution
# until the message topic is up and running and delivers messages
rostopic echo -n 1 /drone$NUMID_DRONE/droneTrajectoryController/controlMode &> /dev/null
rosservice call /drone$NUMID_DRONE/droneRobotLocalizationROSModuleNode/start

gnome-terminal \
`#---------------------------------------------------------------------------------------------` \
`# Executive Coordinator                                                                       ` \
`#---------------------------------------------------------------------------------------------` \
"Executive Coordinator" --command "bash -c \"
roslaunch behavior_coordinator_process behavior_coordinator_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  behavior_catalog_path:=${AEROSTACK_PROJECT}/configs/mission/behavior_catalog.yaml \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  &

sleep 5

gnome-terminal \
`#---------------------------------------------------------------------------------------------` \
`# Behavior Tree Interpreter                                                                   ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Behavior Tree Interpreter" --command "bash -c \"
roslaunch behavior_tree_interpreter behavior_tree_interpreter.launch --wait \
  robot_namespace:=drone$NUMID_DRONE \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT} \
  mission_configuration_folder:=${AEROSTACK_PROJECT}/configs/mission ;
exec bash\""  &

rqt_image_view

#`#---------------------------------------------------------------------------------------------` \
#`# first_view_process                                                                 ` \
#`#---------------------------------------------------------------------------------------------` \
#roslaunch first_view_process first_view.launch --wait robot_namespace:=drone111 drone_console_interface_sensor_overlay_camera:=/hummingbird_adr1/camera_front/image_raw &

#gnome-terminal \
#`#---------------------------------------------------------------------------------------------` \
#`# first_person_viewer_process                                                                 ` \
#`#---------------------------------------------------------------------------------------------` \
#  --tab --title "Pose Adapter" --command "bash -c \"
#roslaunch first_person_view_process first_person_view.launch --wait \
#  robot_namespace:=hummingbird_adr1 \
#  drone_id:=$NUMID_DRONE \
#  drone_console_interface_sensor_overlay_camera:=/hummingbird_adr1/camera_front/image_raw \
#  my_stack_directory:=${AEROSTACK_PROJECT} ;
#exec bash\"" &



