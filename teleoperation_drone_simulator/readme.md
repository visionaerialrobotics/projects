##  Teleoperation with Drone simulator

In order to teleoperate the drone with Drone simulator perform the following steps :


- Execute the roscore:

        $ roscore

- Execute the script that launches the Aerostack components for this project:

        $ ./main_launcher.sh


![teleoperation_drone_simulator.png](https://bitbucket.org/repo/rokr9B/images/2373128517-teleoperation_drone_simulator.png)


- Teleoperate the drone with the alphanumeric interface using the appropriate keys.

Here there is a video that shows a correct execution of the teleoperation:

[ ![Teleoperation with Drone simulator](https://img.youtube.com/vi/8rGHGJ7X4eg/0.jpg)](https://www.youtube.com/watch?v=8rGHGJ7X4eg)
