##  Teleoperation with Rotors simulator


In order to teleoperate the drone with Rotors simulator perform the following steps :

- Execute the script that launches Gazebo for this project:

        $ ./launcher_gazebo.sh

- Execute the script that launches the Aerostack components for this project:

        $ ./main_launcher.sh

- The following window for teleoperation is presented:

![teleoperatrion.png](https://bitbucket.org/repo/rokr9B/images/3175757058-teleoperatrion.png)

- Teleoperate the drone with the alphanumeric interface using the appropriate keys.

Here there is a video that shows the correct execution of the test:

[ ![Python Mission](https://bitbucket.org/repo/rokr9B/images/3175757058-teleoperatrion.png)](https://youtu.be/4Z9bd2xbU3A)
