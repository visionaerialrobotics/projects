#------------------------------------------------------
# FILE: behavior_catalog_aerostack_executive.yaml
#
# BRIEF:
#
#   Catalog of behaviors in Aerostack.
#
# DETAIL:
#
#   Specifies the set of behaviors with two types of
#   information:
#
#   - Behavior descriptors
#
#   - Compatibility constraints
#
#------------------------------------------------------

#------------------------------------------------------
# BEHAVIOR DESCRIPTORS:
#
# For each behavior, a descriptor is specified using
# the following attributes:
#
# category:  A value of {goal_based, recurrent}.
#
# default:   Expresses if the behavior is activated by
#            default or not. Boolean {yes, no}.
#
# timeout:   Maximum available time to reach the goal.
#            Number of seconds (integer).
#
# processes: List of processes that must be started
#            when the behavior is activated.
#
# arguments: List of available arguments for the
#            behavior. For each argument it is possible
#            to define the allowed values, and the
#            dimensions.
#------------------------------------------------------

#------------------------------------------------------
# Default values for behavior attributes.
#------------------------------------------------------
default_values:
  category: goal_based
  default: no
  timeout: 15

#------------------------------------------------------
# LIST OF BEHAVIOR DESCRIPTORS
#------------------------------------------------------
behavior_descriptors:
#------------------------------------------------------
# LOCALIZE_WITH_ODOMETRY
#------------------------------------------------------
  - behavior: LOCALIZE_WITH_ODOMETRY
    category: recurrent
    system: basic_quadrotor_behaviors

#------------------------------------------------------
# KEEP_HOVERING_WITH_PID_CONTROL
#------------------------------------------------------
  - behavior: KEEP_HOVERING_WITH_PID_CONTROL
    system: quadrotor_motion_with_trajectory_controller
    category: recurrent


#------------------------------------------------------
# TAKE_OFF
#------------------------------------------------------
  - behavior: TAKE_OFF
    timeout: 5
    system: basic_quadrotor_behaviors
#------------------------------------------------------
# LAND
#------------------------------------------------------
  - behavior: LAND
    timeout: 5
    system: basic_quadrotor_behaviors
#------------------------------------------------------
# WAIT
#------------------------------------------------------
  - behavior: WAIT
    system: basic_quadrotor_behaviors
    arguments:
      - argument: duration
        allowed_values: [1,1000]
      - argument: until_observed_visual_marker
        allowed_values: [0,1023]

#------------------------------------------------------
# KEEP_MOVING
#------------------------------------------------------
  - behavior: KEEP_MOVING_WITH_PID_CONTROL
    system: quadrotor_motion_with_trajectory_controller
    category:  recurrent
    arguments:
      - argument: speed
        allowed_values: [0,30]
      - argument: direction
        allowed_values: [BACKWARD, FORWARD, UP, DOWN, LEFT, RIGHT]
#------------------------------------------------------
# ROTATE
#------------------------------------------------------
  - behavior: ROTATE_WITH_PID_CONTROL
    system: quadrotor_motion_with_trajectory_controller
    arguments:
      - argument: angle
        allowed_values: [-360,360]
      - argument: relative_angle
        allowed_values: [-360,360]

#------------------------------------------------------
# FOLLOW_PATH
#------------------------------------------------------
  - behavior: FOLLOW_PATH
    system: quadrotor_motion_with_trajectory_controller
    timeout: 240
    arguments:
      - argument: path
        dimensions: 1

#------------------------------------------------------
# GO_TO_POINT_WITH_OCCUPANCY_GRID
#------------------------------------------------------
  - behavior: GO_TO_POINT_WITH_OCCUPANCY_GRID
    system: navigation_with_lidar
    timeout: 240
    arguments:
      - argument: coordinates
        dimensions: 3

#------------------------------------------------------
# GENERATE_PATH_WITH_OCCUPANCY_GRID
#------------------------------------------------------
  - behavior: GENERATE_PATH_WITH_OCCUPANCY_GRID
    system: navigation_with_lidar
    timeout: 240
    arguments:
      - argument: coordinates
        dimensions: 3

#------------------------------------------------------
# SELF_LOCALIZE_AND_MAP_WITH_LIDAR
#------------------------------------------------------
  - behavior: SELF_LOCALIZE_AND_MAP_WITH_LIDAR
    category: recurrent
    timeout: 240
    system: navigation_with_lidar

#------------------------------------------------------
# FOLLOW_PATH_WITH_PID_CONTROL
#------------------------------------------------------
  - behavior: FOLLOW_PATH_WITH_PID_CONTROL
    system: quadrotor_motion_with_trajectory_controller
    timeout: 240
    arguments:
      - argument: path
        dimensions: 1

#------------------------------------------------------
# PAY_ATTENTION_TO_QR_CODE
#------------------------------------------------------
  - behavior: PAY_ATTENTION_TO_QR_CODE
    category: recurrent
    timeout: 1000
    system: attention_to_visual_markers
#------------------------------------------------------
#
# COMPATIBILITY CONSTRAINTS
#
#------------------------------------------------------

#------------------------------------------------------
# EXCLUSIVITY CONSTRAINTS
#------------------------------------------------------
exclusivity_constraints:
#------------------------------------------------------
# Motion behaviors are mutually exclusive
#------------------------------------------------------
- mutually_exclusive:
  - TAKE_OFF
  - LAND
  - KEEP_HOVERING_WITH_PID_CONTROL
  - KEEP_MOVING_WITH_PID_CONTROL
  - ROTATE_WITH_PID_CONTROL
  - FOLLOW_PATH_WITH_PID_CONTROL
  - GO_TO_POINT_WITH_OCCUPANCY_GRID
  - GENERATE_PATH_WITH_OCCUPANCY_GRID

- mutually_exclusive:
  - LOCALIZE_WITH_ODOMETRY
  - SELF_LOCALIZE_AND_MAP_WITH_LIDAR


#------------------------------------------------------
# PRECEDENCE CONSTRAINTS
#------------------------------------------------------
precedence_constraints:
#------------------------------------------------------
# A behavior for self-localization must be active
# before any motion behavior
#------------------------------------------------------
- active:
  before:
    - TAKE_OFF
    - LAND
    - KEEP_HOVERING_WITH_PID_CONTROL
    - ROTATE_WITH_PID_CONTROL
    - FOLLOW_PATH_WITH_PID_CONTROL
    - GO_TO_POINT_WITH_OCCUPANCY_GRID
    - GENERATE_PATH_WITH_OCCUPANCY_GRID
