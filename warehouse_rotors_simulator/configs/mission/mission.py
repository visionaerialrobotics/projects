#!/usr/bin/env python2

import executive_engine_api as api
import rospy
from aerostack_msgs.msg import ListOfBeliefs
import math
import time
qr_codes = []

column1_points = [
  [0.7, -0.4, 1.65],
  [0.7, 10, 1.65],
  [0.7, 10, 0.55],
  [0.7, -0.4, 0.55]
]

column2_points = [
  [4.2, -0.4, 0.55],
  [4.2, 10, 0.55],
  [4.2, 10, 1.65],
  [4.2, -0.4, 1.65],
  [1, -0.5, 1.5]
]

points = [
  column1_points,
  column2_points
]

def qr_callback(msg):
  global qr_codes
  index = msg.beliefs.find('code(')

  if not index == -1:
    substring = msg.beliefs[index+10:index+12]  

    if not substring.isdigit():
      substring = substring[:-1]

    if qr_codes.count(substring) == 0:
      qr_codes.append(substring)
      print('QR code: {}'.format(substring))


def runMission():
  global qr_codes
  print("Starting mission...")
  print("Taking off...")
  api.activateBehavior('SELF_LOCALIZE_AND_MAP_WITH_LIDAR')
  api.executeBehavior('TAKE_OFF')
  activated, uid = api.activateBehavior('PAY_ATTENTION_TO_QR_CODE')
  if not activated:
    raise Exception('Unable to active the QR recognizer')
  j=0
  verticalPoint=1
  rospy.Subscriber("/drone111/all_beliefs", ListOfBeliefs, qr_callback)
  for destination in points:
    for point in destination:
     if(j==4 or j==8):
      print("Generating path")
      result = api.executeBehavior('GENERATE_PATH_WITH_OCCUPANCY_GRID', coordinates=point)
      query = 'path ('+str(verticalPoint)+',?y)'.format()
      verticalPoint+=1
      success , unification = api.consultBelief(query)
      print query
      print success
      if success:
         traject = unification['y']
         traject = eval(traject)
         traject = list(traject)
         result = [] 
         i=0
         pointTraj=traject[0]
         dist=0.75
         while i < len(traject)-1: 
                 pointTraj= traject[i]
                 result.append(list(traject[i])) 
                 i+=1
         result.append(list(traject[len(traject)-1])) 
         print len(result)
         print "Following path"
         print "---------------------------------"
      result = api.executeBehavior('FOLLOW_PATH_WITH_PID_CONTROL', path=result)
      time.sleep(3)
      j+=1
     else:
      result = api.executeBehavior('GO_TO_POINT_WITH_OCCUPANCY_GRID', coordinates=point)
      j+=1
      time.sleep(2)

  #print(qr_codes)
  print('-> Total QR codes detected: {}'.format(len(qr_codes)))

  result = api.executeBehavior('LAND')
  print('-> result {}'.format(result))
  print('Finish mission...')