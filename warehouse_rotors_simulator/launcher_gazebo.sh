#!/bin/bash

DRONE_SWARM_MEMBERS=$1
export AEROSTACK_PROJECT=${AEROSTACK_STACK}/projects/warehouse_rotors_simulator
if [ -z $DRONE_SWARM_MEMBERS ] # Check if NUMID_DRONE is NULL
  then
    #Argument 1 empty
      echo "-Setting Swarm Members = 1"
      DRONE_SWARM_MEMBERS=1
  else
      echo "-Setting DroneSwarm Members = $1"
fi

gnome-terminal  \
    --tab --title "DroneRotorsSimulator" --command "bash -c \"
roslaunch ${AEROSTACK_PROJECT}/rotors_files/launch/env_mav_rl_navigation.launch --wait project:=${AEROSTACK_PROJECT};
            exec bash\""  &

for (( c=1; c<=$DRONE_SWARM_MEMBERS; c++ ))
do  
gnome-terminal  \
    --tab --title "DroneRotorsSimulator" --command "bash -c \"
roslaunch rotors_gazebo mav_swarm_rl_navigation.launch --wait drone_swarm_number:=$c mav_name:=hummingbird_laser;
            exec bash\""  &
done


