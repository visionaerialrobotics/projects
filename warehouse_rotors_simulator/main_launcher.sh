#!/bin/bash

NUMID_DRONE=111
DRONE_SWARM_ID=1
export AEROSTACK_PROJECT=${AEROSTACK_STACK}/projects/warehouse_rotors_simulator

#---------------------------------------------------------------------------------------------
# INTERNAL PROCESSES
#---------------------------------------------------------------------------------------------
gnome-terminal  \
`#---------------------------------------------------------------------------------------------` \
`# Driver rotors                                                                               ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Driver Rotors" --command "bash -c \"
roslaunch driverRotorsSimulatorROSModule driverRotorsSimulatorROSModuleSim.launch --wait \
  drone_id_int:=$NUMID_DRONE \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_swarm_id:=$DRONE_SWARM_ID \
  mav_name:=hummingbird_laser \
  my_stack_directory:=${AEROSTACK_PROJECT};
            exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Midlevel Controller                                                                         ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Midlevel Controller" --command "bash -c \"
roslaunch droneMidLevelAutopilotROSModule droneMidLevelAutopilotROSModule.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
            exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Robot Localization                                                                          ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "RobotLocalizationROSModule" --command "bash -c \"
roslaunch droneRobotLocalizationROSModule droneRobotLocalizationROSModule.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
            exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# EKF Localization                                                                            ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Robot localization" --command "bash -c \"
roslaunch ${AEROSTACK_STACK}/launchers/rotors_simulator_launchers/launch_files/Ekf.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
            exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Trajectory Controller                                                                       ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Trajectory Controller" --command "bash -c \"
roslaunch droneTrajectoryControllerROSModule droneTrajectoryControllerROSModule.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT} \
  drone_estimated_pose_topic_name:=EstimatedPose_droneGMR_wrt_GFF \
  drone_estimated_speeds_topic_name:=EstimatedSpeed_droneGMR_wrt_GFF;
    exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Move Base                                                                                   ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Move Base" --command "bash -c \"
roslaunch ${AEROSTACK_STACK}/launchers/move_base_launcher/move_base.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
            exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Hector Slam                                                                                 ` \
`#---------------------------------------------------------------------------------------------` \
    --tab --title "Hector Slam" --command "bash -c \"
roslaunch ${AEROSTACK_STACK}/launchers/hector_slam_launchers/hector_slam.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$ \
  my_stack_directory:=${AEROSTACK_STACK};
            exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Process Monitor                                                                             ` \
`#---------------------------------------------------------------------------------------------` \
  --tab --title "Process Monitor" --command "bash -c \"
roslaunch  process_monitor_process process_monitor.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
            exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Process Manager                                                                             ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Process manager" --command "bash -c \"
roslaunch process_manager_process process_manager_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Path Planner                                                                                ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Path Planner"  --command "bash -c \"
roslaunch path_planner_in_occupancy_grid path_planner_in_occupancy_grid.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT} debug:=true;
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Belief Manager                                                                              ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Belief Manager" --command "bash -c \"
roslaunch belief_manager_process belief_manager_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT} \
    config_path:=${AEROSTACK_PROJECT}/configs/drone$NUMID_DRONE;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Belief Updater                                                                              ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Belief Updater" --command "bash -c \"
roslaunch belief_updater_process belief_updater_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT} \
    config_path:=${AEROSTACK_PROJECT}/configs/drone$NUMID_DRONE;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# QR Code Recognizer                                                                          ` \
`# Recognizes QR Codes                                                                         ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Recognize QR" --command "bash -c \"
roslaunch qr_recognizer qr_recognizer.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id_int:=$NUMID_DRONE \
    interface_sensor_front_camera:=camera_front/image_raw \
    my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\"" \
`#---------------------------------------------------------------------------------------------` \
`# Python Interpreter                                                                          ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Python Interpreter" --command "bash -c \"
roslaunch python_based_mission_interpreter_process python_based_mission_interpreter_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT} \
  mission_configuration_folder:=${AEROSTACK_PROJECT}/configs/mission ;
exec bash\""  \
`#---------------------------------------------------------------------------------------------` \
`# Self Localization Selector Process                                                          ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Self Localization Selector" --command "bash -c \"
roslaunch self_localization_selector_process self_localization_selector_process.launch --wait \
    drone_id_namespace:=drone$NUMID_DRONE \
    drone_id:=$NUMID_DRONE \
    my_stack_directory:=${AEROSTACK_PROJECT} \
    aruco_slam_estimated_speed_topic:=EstimatedSpeed_droneGMR_wrt_GFF;
exec bash\""  &

echo "- Waiting for all process to be started..."
# wait for the modules to be running, the trick here is that rostopics blocks the execution
# until the message topic is up and running and delivers messages
rostopic echo -n 1 /drone$NUMID_DRONE/droneTrajectoryController/controlMode &> /dev/null
rosservice call /drone$NUMID_DRONE/droneRobotLocalizationROSModuleNode/start


gnome-terminal \
`#---------------------------------------------------------------------------------------------` \
`# Navigation With Lidar Behaviors                                                             ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Navigation With Lidar" --command "bash -c \"
roslaunch navigation_with_lidar navigation_with_lidar.launch --wait \
  namespace:=drone$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""&
gnome-terminal \
`#---------------------------------------------------------------------------------------------` \
`# Basic Quadrotor Behaviors                                                                   ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Basic Quadrotor Behaviors" --command "bash -c \"
roslaunch basic_quadrotor_behaviors basic_quadrotor_behaviors.launch --wait \
  namespace:=drone$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""&
gnome-terminal \
`#---------------------------------------------------------------------------------------------` \
`# Quadrotor Behaviors With Trajectory Controller                                              ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Basic Quadrotor Behaviors" --command "bash -c \"
roslaunch quadrotor_motion_with_trajectory_controller quadrotor_motion_with_trajectory_controller.launch --wait \
  namespace:=drone$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""&
gnome-terminal \
`#---------------------------------------------------------------------------------------------` \
`# Attention To Visual Markers                                                                 ` \
`#---------------------------------------------------------------------------------------------` \
--tab --title "Attention to visual markers" --command "bash -c \"
roslaunch attention_to_visual_markers attention_to_visual_markers.launch --wait \
  namespace:=drone$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""&

#---------------------------------------------------------------------------------------------
# SHELL INTERFACE
#---------------------------------------------------------------------------------------------
gnome-terminal  \
--tab --title "DroneInterface"  --command "bash -c \"
roslaunch droneInterfaceROSModule droneInterface_jp_ROSModule.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  drone_id_int:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  &

gnome-terminal \
`#---------------------------------------------------------------------------------------------` \
`# Executive Coordinator                                                                       ` \
`#---------------------------------------------------------------------------------------------` \
"Executive Coordinator" --command "bash -c \"
roslaunch behavior_coordinator_process behavior_coordinator_process.launch --wait \
  drone_id_namespace:=drone$NUMID_DRONE \
  behavior_catalog_path:=${AEROSTACK_PROJECT}/configs/mission/behavior_catalog.yaml \
  drone_id:=$NUMID_DRONE \
  my_stack_directory:=${AEROSTACK_PROJECT};
exec bash\""  &

`#---------------------------------------------------------------------------------------------` \
`# Rviz (To show the occupancy_grid)                                                           ` \
`#---------------------------------------------------------------------------------------------` \
rosrun rviz rviz -d ${AEROSTACK_PROJECT}/configs/drone$NUMID_DRONE/mapping.rviz&
